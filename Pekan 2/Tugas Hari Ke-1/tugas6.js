// Soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]
var objDaftarPeserta = {
    nama: arrayDaftarPeserta[0],
    "jenis kelamin": arrayDaftarPeserta[1],
    hobi: arrayDaftarPeserta[2],
    "tahun lahir": arrayDaftarPeserta[3]
}
console.log(objDaftarPeserta);

// Soal 2
var buah = [{
        nama: 'strawberry',
        warna: 'merah',
        'ada bijinya': 'tidak',
        harga: 9000
    },
    {
        nama: 'jeruk',
        warna: 'oranye',
        'ada bijinya': 'ada',
        harga: 8000
    },
    {
        nama: 'semangka',
        warna: 'Hijau & merah',
        'ada bijinya': 'ada',
        harga: 10000
    },
    {
        nama: 'pisang',
        warna: 'kuning',
        'ada bijinya': 'tidak',
        harga: 5000
    }
]

console.log(buah[0]);

// Soal 3
var dataFilm = []

function addDataFilm(nama, durasi, genre, tahun) {
    var film = {};
    film.nama = nama;
    film.durasi = durasi;
    film.genre = genre;
    film.tahun = tahun;
    dataFilm.push(film);
    return film;
}
var film1 = addDataFilm('Bucin', 'Belum tau','Komedi',2020)
console.log(dataFilm);

// Soal 4
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs = 2   
         
    }
    get namaKera(){
        return this.nama
    }
    yell(){
        return "Auooo"
    }
}
class Frog extends Animal{
    constructor(name){
        super(name)    
    }
    get namaKodok(){
      return this.nama
    } 
    jump(){
    return "hop hop"
}
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log(sungokong);
console.log(kodok);

console.log(sheep)


// Soal 5

class Clock{
    constructor({ template }) {
      
      var timer;
    
      function render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
      }
    
      this.stop = function() {
        clearInterval(timer);
      };
    
      this.start = function() {
        render();
        timer = setInterval(render, 1000);
      };
    
    }
    }
    var clock = new Clock({template: 'h:m:s'});
    clock.start(); 
    