// SOAL 1

//LUAS LINGKARAN
let jari = 10
const luasLingkaran = jari => 3.14 * jari * jari;
console.log(luasLingkaran(jari));

//KELILING LINGKARAN
let diameter = 20
const kelilingLingkaran = diameter => 3.14 * diameter;
console.log(kelilingLingkaran(diameter));


// SOAL 2
let kalimat = ""

const tambahKata = (satu, dua, tiga, empat, lima) => {

    kalimat = `${satu} ${dua} ${tiga} ${empat} ${lima}`

    return kalimat;

}
console.log(tambahKata('saya', 'adalah', 'seorang', 'frontend', 'developer'));

// SOAL 3

class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}
class Komik extends Book {
    constructor(name, totalPage, price) {
        super()
        this.name = name
        this.totalPage = totalPage
        this.price = price
        this.isColorful = true
    }
    show() {
        return "nama buku " + this.name + ", total halaman " + this.totalPage + ", harga buku " + this.price
    }
}
buku = new Komik("bacaan", 10, 10000)
console.log(buku.show())