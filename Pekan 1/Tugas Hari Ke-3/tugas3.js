// soal pertama
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kapital = kataKedua[0].toUpperCase()+kataKedua.slice(1);
var besar = kataKeempat.toUpperCase();

console.log(kataPertama+" "+kapital+" "+kataKetiga+" "+besar);

// soal kedua
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var num1 = Number(kataPertama);
var num2 = Number(kataKedua);
var num3 = Number(kataKetiga);
var num4 = Number(kataKeempat);

console.log(num1+num2+num3+num4);

// soal ketiga 
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal keempat
var nilai;
nilai = 70;

if (nilai >= 80) {
    console.log("Indeksnya A")  
} else if (nilai >= 70 && nilai < 80) {
    console.log("Indeksnya B")  
} else if (nilai >= 60 && nilai < 70) {
        console.log("Indeksnya C")
}else if (nilai >= 50 && nilai < 60) {
            console.log("Indeksnya D")   
}else if (nilai < 50) {
        console.log("Indeksnya E")   
    } 

// soal kelima 
var tanggal = 21;
var bulan = 08;
var tahun = 1998;

switch (bulan) {
    case 01:
      console.log(tanggal + " Januari " + tahun);
      break;
    case 02:
      console.log(tanggal + " Februari " + tahun);
      break;
    case 03:
      console.log(tanggal + " Maret " + tahun);
      break;
    case 04:
      console.log(tanggal + " April " + tahun);
      break;
      case 05:
        console.log(tanggal + " Mei" + tahun);
        break;
    case 06:
       console.log(tanggal + " Juni " + tahun);
      break;
    case 07:
       console.log(tanggal + " Juli " + tahun);
      break;
    case 08:
       console.log(tanggal + " Agustus " + tahun);
      break;
    case 09:
       console.log(tanggal + " September " + tahun);
      break;
    case 10:
       console.log(tanggal + " Oktober " + tahun);
      break;
    case 11:
       console.log(tanggal + " November " + tahun);
      break;
    case 12:
       console.log(tanggal + " Desember " + tahun);
      break;

      default:
        console.log("Tidak ada yang cocok "+ tanggal + bulan + tahun);
  }
