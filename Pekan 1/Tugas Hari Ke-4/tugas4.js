// soal 1
console.log("---Soal 1---");

var nilaiAwal = 1;
var awal = 'lopping pertama';
var kedua = 'lopping kedua';

console.log(awal.toUpperCase());
while (nilaiAwal <= 20) {
    if (nilaiAwal % 2 == 0) {
        console.log(nilaiAwal + ' - I love coding');
    } else {

    }
    nilaiAwal++;
}

console.log(kedua.toUpperCase());
while (nilaiAwal > 0) {
    if (nilaiAwal % 2 == 0) {
        console.log(nilaiAwal + ' - I will become a frontend developer');
    } else {

    }
    nilaiAwal--;
}

// soal 2
console.log("--Soal 2---");

for (i = 0; i <= 20; i++) {
    if ((i % 3 == 0) && (i % 2 == 1)) {
        console.log(i + ' - I love coding')
    } else if (i % 2 == 1) {
        console.log(i + ' - Santai');
    } else if (i % 2 == 0) {
        console.log(i + ' - Berkualitas')
    }
}

// soal 3
console.log("---Soal 3---");
var s = '';
for (var i = 1; i <= 7; i++) {
    for (var j = 1; j < i; j++) {
        s += '*';
    }
    s += '\n';
}
console.log(s);

// soal 4
console.log("---Soal 4---");
var kalimat = "saya sangat senang belajar javascript";
var kalimat1 = kalimat.split(" ");
console.log(kalimat1);

// soal 5
console.log("---Soal 5---");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sort = daftarBuah.sort();
var i;
for (i = 0; i <= 4 ; i++){
    console.log(sort[i]);
}